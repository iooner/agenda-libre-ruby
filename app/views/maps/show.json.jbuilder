json.array!(@orgas) do |orga|
  json.merge!(
    type: 'Feature',
    properties: {
      id: orga.id,
      icon: @kind.icon,
      name: orga.name,
      place_name: orga.place_name, address: orga.address, city: orga.city,
      region: orga.region.name, region_id: orga.region_id,
      tags: orga.tag_list,
      popupContent: link_to(orga, orga_url(orga))
    },
    geometry: {
      type: 'Point',
      coordinates: [orga.longitude, orga.latitude]
    }
  )
end
