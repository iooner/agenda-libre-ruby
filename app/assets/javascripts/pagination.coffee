# Automatic ajax pagination
$(document).on 'turbolinks:load', ->
	$('.pagination .next a').attr('data-remote', true).each ->
		# Go to the next page when page is scrolled
		$(document).scroll =>
			if $(this).visible true, true
				# "Next" link is also hidden while pagination is done
				$(this).click().parents('.pagination').hide()

$(document).on 'ajax:success', '.pagination .next a', (event, data) ->
	$(this).parents('tfoot').prev().append $('tbody tr', data)

	next = $('.pagination .next a', data).attr 'href'
	if next?
		$(this).attr('href', next).parents('.pagination').show()
