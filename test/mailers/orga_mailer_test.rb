require 'test_helper'

# Test and check how the organisations mailer is working
class OrgaMailerTest < ActionMailer::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    @config = Rails.application.config
  end

  test 'create' do
    mail = OrgaMailer.create Orga.last
    assert_match(/Organisation .* en attente de modération/,
                 mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_equal [@config.action_mailer.default_options[:from]],
                 mail.from
    assert_match 'Bonjour', mail.body.encoded
  end

  test 'update' do
    @orga = Orga.last
    # Necessary to have the proper paper_trail version
    @orga.update name: 'My Title'
    mail = OrgaMailer.update @orga
    assert_match(/Organisation .* modifiée/, mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_equal [@config.action_mailer.default_options[:from]],
                 mail.from
    assert_match 'Bonjour', mail.body.encoded
  end

  test 'accept' do
    mail = OrgaMailer.accept Orga.last
    assert_match(/Organisation .* modérée/, mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_equal [@config.action_mailer.default_options[:from]],
                 mail.from
    assert_match 'Bonjour', mail.body.encoded
  end

  test 'destroy' do
    mail = OrgaMailer.destroy Orga.last, 'hello world'
    assert_match(/Organisation .* supprimée/, mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_equal [@config.action_mailer.default_options[:from]],
                 mail.from
    assert_match 'Bonjour', mail.body.encoded
  end
end
